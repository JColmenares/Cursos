// Variables
    const carrito = document.getElementById('carrito');
    const cursos = document.getElementById('lista-cursos');
    const listaCursos = document.querySelector('#lista-carrito tbody');
    const vaciarCarritoBtn = document.getElementById('vaciar-carrito');




// Listeners

cargarEventListeners();

function cargarEventListeners(){
    // Dispara cuando se presiona "Agregar Carrito"
    cursos.addEventListener('click', comprarCurso);

    // Cuando se elimina un curso del carrito
    carrito.addEventListener('click', eliminarCurso);

    // Al vaciar el carrito
    vaciarCarritoBtn.addEventListener('click', vaciarCarrito);

    // Al cargar el documento, mostrar LocalStorage
    document.addEventListener('DOMContentLoaded', leerLocalStorage);
}


// Funciones



// Función que añade curso al carrito
function comprarCurso(e){
    e.preventDefault();
    // Delegation para agregar-carrito
    if(e.target.classList.contains('agregar-carrito')){   
        const curso = e.target.parentElement.parentElement; 
        // Enviamos el curso seleccionado para tomar sus datos
        leerDatosCurso(curso);
    }
}

// Lee los datos del curso
function leerDatosCurso(curso) {
    const infoCurso = {
        imagen: curso.querySelector('img').src,
        titulo: curso.querySelector('h4').textContent,
        precio: curso.querySelector('.precio span').textContent,
        id: curso.querySelector('a').getAttribute('data-id'),
        
    }

    insertarCarrito(infoCurso);
}

// Muestra el curso seleccionado en el Carrito
function insertarCarrito(curso){
    const row = document.createElement('tr');
    row.innerHTML = `
         <td>
            <img src="${curso.imagen}" width=100>
         </td>
         <td>${curso.titulo}</td>
         <td>${curso.precio}</td>
         <td>
            <a href="#" class="borrar-curso" data-id="${curso.id}">X</a>
         </td>
    `;
    listaCursos.appendChild(row);
    guardarCursoLocalStorage(curso);
}

// Elimina el curso del carrito en el DOM
function eliminarCurso(e){
    e.preventDefault();
    let curso,
        cursoId;
    if(e.target.classList.contains('borrar-curso')){
        e.target.parentElement.parentElement.remove();
        curso = e.target.parentElement.parentElement;
        cursoId = curso.querySelector('a').getAttribute('data-id');
    }
    eliminarCursoLocalStorage(cursoId);
}

// Elimina los cursos del carrito en el DOM
function vaciarCarrito() {
    // Forma lenta
    //listaCursos.innerHTML = '';
    // Forma rapida (recomendada)
    while(listaCursos.firtChild) {
        listaCursos.removeChild(listaCursos.firstChild);
    }

    // Vaciar Local Storage
    vaciarLocalStorage();

    return false;// Evita el salto que puede generar la vista.

    //NOTA: En la comparación de ambas formas de eliminar elementos del DOM recomiendan el while que el innerHTML, ya que es 10 veces más rapido que el otro (Link donde realizan la comparación - https://jsperf.com/innerhtml-vs-removechild)

}

function guardarCursoLocalStorage(curso){
    let cursos;
    // Toma el valor de un arreglo con datos de LS o vacio 
    cursos = obtenerCursoLocalStorage();

    // El curso seleccionado se agrega al arreglo
    cursos.push(curso);

    localStorage.setItem('cursos', JSON.stringify(cursos));
}

// Comprueba que haya elementos en Local Storage
function obtenerCursoLocalStorage(){
    let cursosLS;

    // Comprobamos si hay algo en localStorage
    if(localStorage.getItem('cursos') === null){
        cursosLS = [];
    } else {
        cursosLS = JSON.parse( localStorage.getItem('cursos'));
    }
    return cursosLS;
}

// impremir los cursos de Local Storage en el carrito

function leerLocalStorage(){
    let cursosLS;

    cursosLS = obtenerCursoLocalStorage();

    cursosLS.forEach(function(curso){
        // Construir el templace
        const row = document.createElement('tr');
    row.innerHTML = `
         <td>
            <img src="${curso.imagen}" width=100>
         </td>
         <td>${curso.titulo}</td>
         <td>${curso.precio}</td>
         <td>
            <a href="#" class="borrar-curso" data-id="${curso.id}">X</a>
         </td>
    `;
    listaCursos.appendChild(row);

    });
}

function eliminarCursoLocalStorage(curso){
    let cursosLS;
    // Obtenemos el arreglo de cursos
    cursosLS = obtenerCursoLocalStorage();
    // Iteramos comparando el ID del curso borrado con los del LS
    cursosLS.forEach(function(cursoLS, index) {
        if(cursoLS.id ===curso) {
            cursosLS.splice(index, 1);
        }
    });
    // Añadimos el arreglo actual al Local Storage
    localStorage.setItem('cursos', JSON.stringify(cursosLS));
}

// Eliminar todos los cursos de Local Storage

function vaciarLocalStorage(){
    localStorage.clear();
}