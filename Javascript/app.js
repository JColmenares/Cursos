///CARPETA FUNDAMENTOS DE JAVASCRIPT///



//Video 5
    //document.getElementById('app').innerHTML = "Hola mundo";

    //let nombre = prompt('Cual es tu nombre?'); //Variables 
    //let edad =prompt('Cual es tu edad?');

    //document.getElementById('app').innerHTML = `Bienvenido ${nombre} de ${edad} años`; //Imprime información al usuario con los atributos id

//Video 7 y 8 de uso de consola
    //console.log("Enviado a la consola"); //Envia valores a la casola de chrome
    //console.log(2 + 2);
    //console.log('hasta aqui todo bien');
    //console.log(true);
    //console.log('ID: ' + 20);

//Video 9 - Creando Variables

    // var let const 
    // VAR
        var nombre = 'Juan';
        nombre = 'jose';
        var nombre = 'jorge';
        var Var;

        var carrito = 'variable_1', carrito_2 = 'variable_2'

        console.log(nombre);
        console.log(Var);
        console.log(carrito,carrito_2);

        var primerNombre = 'Juan' // camelcase
        var primer_nombre = 'Juan' // undercore
        var PrimerNombre = 'Juan' // pascal case


        //NOTA: Las variables no pueden llamarse con numeros ni caracteres especiales al comienzo, pero luego de una letra si se puede colocar.

    /* Otra forma de comentar*/

//Video 10 - Continuación Variables
    // LET
        let nombres;
        nombres = 'Juan';
        let productos = 'Libro';

        let nombres_1 = 'Juan', productos_1 = 'Otro producto';

        console.log(nombres);

        //NOTA: una variable declarada con let solo se debe ser declarada una vez, de lo contrario soltara error, si se quiere cambiar el valor, solo se llama en nombre de la variable y se le asigna el valor.

    // CONST

        const produt = 'Libro';

        console.log(produt);

        //NOTA: Este tipo de variable siempre deben ser inicializadas con un valor, dicho valor no se puede modificar, osea siempre sera fija.

//Video 11 - Continuación Variables

    //Variables con valor string - cadena de texto
    let nombre_3,mensaje;
    nombre_3 = 'Pedro'; // Mas recomendado
    nombre_3 = "Pedro";

    mensaje = 'Y entonces dije "Buen Curso!!"';
    mensaje = 'Y entonces dije \'Buen Curso!!\' '; //Esto se llama escapar las comillas.

    console.log(mensaje);


    //Concatenar 
    let conca = 'JAVA' + 'SCRIPT'
    console.log(conca);

    let aprendiendo = "Aprendiendo", tecnologia = 'JavaScript';

    console.log(aprendiendo + " " + tecnologia); //Forma vieja de hacerlo
    console.log(`${aprendiendo} ${tecnologia}`); //Forma nueva

//Video 12 y 13 - Cadenas de texto y sus metodos

    let cadena = "Prueba";

    console.log(cadena.length); //Para saber la longitud del la cadena de caracteres.

    console.log(tecnologia.concat(' ','Es genial')); //Otra forma de concatenar string (Casi no se usa).

    console.log(tecnologia.toUpperCase()); //Colocar en mayuscula la cadena de texto.

    console.log(tecnologia.toLowerCase()); //Colocar en minuscula la cadena de texto.

    let mensaje_2 = "Aprendiendo JavaScript, CSS, HTML para ser Frontend";

    console.log(mensaje_2.indexOf('JavaScript')); //Busca en que posición se encuentra una palabra o letra en una cadena de caracteres, de no encontrarla devuelve -1.

    console.log(mensaje_2.substring(0,11)); //Permite seleccionar con los parametros la posición de la linea de los caracteres que se requieren (Con inicio y fin).

    console.log(mensaje_2.slice(-3)); //Permite seleccionar con el parametros, desde derecha a izquierda la posición de la linea de los caracteres que se requiere. Pero tambien trabaja de izquierda a derecha colocando inicio y fin (Trabaja igual que .substring).

    console.log(mensaje_2.split(' ')); //Permite separar una cadena de texto en arreglos o listas (Como se trabaja en python).

    console.log(mensaje_2.replace('CSS', 'PHP')); //Permite reemplazar texto de una cadena de texto.

    console.log(mensaje_2.includes('CSS')); //Permite buscar una texto en una cadena de texto.

    let var_1 = "JavaScript" + ' ';

    console.log(var_1.repeat(5)); //Permite repetir tantas veces se desee una cadena de texto.

//Video 14 y 15 - Operaciones con numeros

    const numero_1 = 30,
          numero_2 = 20,
          numero_3 = 20.20,
          numero_4 = .1020,
          numero_5 = -3;
    
    console.log(numero_1);

    let resultado;

    //Suma
        resultado = numero_1 + numero_2;
    //Restar
        resultado = numero_1 - numero_2;
    //Multiplicar
        resultado = numero_1 * 2;
    //División
        resultado = numero_1 / numero_2;
    //Modulo (Residuo de una división)
        resultado = numero_1 % numero_2;
    //Pi
        resultado = Math.PI;
    //Redondeo
        resultado = Math.round(2.5); //Redondeo normal
        resultado = Math.ceil(2.1); //Redondea para arriba
        resultado = Math.floor(2.1); //Redondea para abajo
    //Raiz cuadrada
        resultado = Math.sqrt(144);
    //Absoluto
        resultado = Math.abs(numero_5);
    //Potencia
        resultado = Math.pow(8,3);
    //Minimos
        resultado = Math.min(3,6,5,1,9,7,8);
    //Maximo
        resultado = Math.max(3,6,5,1,9,7,8);
    //Aleatorio
        resultado = Math.random();

    //Forma que se realizan las operaciones
        resultado = (10 + 20) * 5;

        resultado = (10 + 20 + 20 + 10 + 40) * .20;

    console.log(resultado);

//Video 16 - Tipo de Datos

    let valor;

    valor = 'Cadena de texto'; //String
    valor = 20;                //Number
    valor = true;              //Booleand
    valor = null               //Object
    valor = undefined;         //Undefiend
    //Objecto
    valor = {nombre: 'juan'};  //Object
    valor = Symbol('Simbolo'); //Symbol
    //Arreglo
    valor = [1,2,3,4,5];       //Object
    //Fecha
    valor = new Date();        //Object

    console.log(valor);

    console.log(typeof valor); //Permite ver que tipo de valor contiene la variable (String, Number, Object, Boolean, Undefined, Symbol).

    //NOTA: Todos los números son de valor tipo number

//Video 17 - Operadores de Comparación

    const numero1 = 20;
    const numero2 = 50;
    const numero3 = '20';

    //Comparador Mayor que
    console.log( numero1 > numero2 );
    console.log( 'a' > 'b' );//ABCDE....yxz
    //Comparador Menor que
    console.log( numero1 < numero2 );
    //comparador Igual
    console.log( numero1 == numero2 ); //Validad los valores nada mas a ver si son iguales.
    console.log( numero1 === numero2 ); //Validad los valores  y si su tipo de variable son iguales.
    console.log( 'hola' == 'hola' ); //Validad los valores nada mas a ver si son iguales.
    //Diferente
    console.log(2 != 3);
    
    //NOTA: Javascript permite comparar las letras según su posición en el abecedario, las letras minusculas tienen mas peso o valor que las letras Mayusculas.


//Video 18 y 19 - convertir string a numero

    const num1 = "50",
          num2 = 10,
          num3 = 'tres';
          
    console.log( Number(num1) + num2 );
    console.log(parseInt(num1) + num2); //Otra forma de convertir string a numero.

    //NOTA: Si se quiere convertir una variable Number a Number, sin el typeof aparecera un error de NaN
    console.log(parseInt(num3));

    let dato;
    dato = Number("20");
    dato = Number('20.10931');
    dato = Number(true); //Devuelve 1
    dato = Number(false);//Devuelve 0
    dato = Number(null); //Devuelve 0
    dato = Number(undefined);//Devuelve NaN.
    dato = Number('hola mundo');//Devuelve NaN.
    dato = Number([1,2,3,4]);//Devuelve NaN.

    // ParseInt y ParseFloat
    dato = parseInt('100');
    dato = parseFloat('100');
    dato = parseFloat('100.201');
    dato = parseInt('250.362');//Elimina los decimales.
    
    console.log(dato);

    // tofixed

    dato = 18500
    dato = 18520.32165
    console.log(dato.toFixed(3));//Permite seleccionar cuantos decimales se desean ver en el valor (Solo funciona en números).

    dato = '18523.586874'
    console.log(parseFloat(dato).toFixed(2));
    console.log(' ');

//Video 20 - Coversion de Number a string
    
    let cp;
    cp = 90210;
    cp = String(cp);
    
    console.log(cp.length)

    let datos;
    datos = '4' + '4';

    //Booleanos
    datos = true
    datos = false
    datos = String(datos);

    //De arreglo a string
    datos = String([1,2,3]);

    //toString();
    datos = 20;
    datos = true;
    datos = [1,2,3,4];
    datos = datos.toString();

    console.log(datos);
    console.log(datos.length);
    console.log(typeof datos);

    console.log(' ');

//Video 21 - Template Literal o String Template

    const producto1 = 'Pizza',
          precio1 = 30,
          producto2 = 'Hamburguesa',
          precio2 = 40;

    let html;
    /*
    html = '<ul>' +
            '<li>Orden: ' + producto1 + '</li>' +
            '<li>Precio: ' + precio1 + '</li>' +
            '<li>Orden: ' + producto2 + '</li>' +
            '<li>Precio: ' + precio2 + '</li>' +
            '<li>Total: ' + (precio1 + precio2) + '</li>' +
            '</ul>'; //Forma vieja de trabajar

    //console.log(html);
    */

    html = `
            <ul>
                <li>Orden: ${producto1} </li>
                <li>Precio: ${precio1} </li>
                <li>Orden: ${producto2} </li>
                <li>Precio: ${precio2} </li>
                <li>Total: ${total(precio1, precio2)} </li>
            
            </ul>`;

    function total(precio1, precio2) {
        return precio1 + precio2;
    }
            
    document.getElementById('app').innerHTML = html;

//Video 22, 23 y 24 - Arreglos y sus métodos

    const nums = [10,20,30,40,50];
    console.log(nums);

    //Arreglos de String (método alterno)
    const meses = new Array('Enero','Febrero','Marzo','Abril');
    console.log(meses[2]);
    console.log(meses.length);
    console.log(Array.isArray(meses));//Para comprobar que es un arreglo.

    //Añadir elementos al Arreglo
        //Añadir en arreglo
        meses[4] = 'Mayo';
        meses.push('Julio');//Sirve para agregar con tenido en los arreglos al final.

        //Añadir al inicio del arreglo
        meses.unshift('Mes 0');
        console.log(meses);

    //Eliminar elemento de un arreglo
        //El ultimo
        meses.pop();
        console.log(meses);
        //El primero
        meses.shift();
        console.log(meses);
        //Quitar un rango
        meses.splice(2,1); //.splice(Donde comienza elemento,cantidad).
        console.log(meses);
        //Revertir
        meses.reverse();
        console.log(meses);


    //Arreglo mezclados
    const mezclado = ['hola',20,true,null,undefined];
    console.log(mezclado);

    //Unir un arreglo con otro
    let arreglo1 = [1,2,3],
        arreglo2 = [9,8,7];

    console.log(arreglo1.concat(arreglo2));

    //Encontrar un elemento en el arreglo
    console.log(meses.indexOf('Abril'));


    //Ordenar un arreglo
    const frutas = ['Platano','Manzana','Fresa','Naranja'];
        //Letras Ascendente
        frutas.sort();
        console.log(frutas);
        //Numeros
        arreglo3 = [3,9,91,92,93,23,45,21,56,1,100,10];
        arreglo3.sort();
        console.log(arreglo3);
        //NOTA: Ordena desde el primero numero y así sucesivamente

        arreglo3.sort(function(x,y){
            return x - y; //Ascendemte, y Descendente (y - x).
        });
        console.log(arreglo3);
        //NOTA: con esta función se permite ordenar un arreglo adecuadamente.

//Video 25 - Variableconst en objetos y arreglos.

    const num = [1,2,3];
    num[0] = 4;
    num.push(5);
    console.log(num);

    //NOTA: Los arreglos const pueden ser modificados solo en valores individuales, como tambien pueden ser agregados (Trabajan como las tuplas en python), no se puede reasignar el arreglo.

//Video 26 - Objetos
    const persona = {
        nombre: 'miguel',
        apellido: 'Martinez',
        profesion: 'Diseñador gráfico',
        email: 'correo@correo.com',
        edad: 20,
        musica: ['Trance','Rock','Grunge'],
        hogar: {
            ciudad: 'Guafalajara',
            pais:'Mexico',
        },
        nacimiento: function() {
            return new Date().getFullYear() - this.edad;//this sirve para acceder sus mismas propiedades.
        }

    };
    persona.musica.push('Alternativo');//Permite agregar valores a un arreglo dentro de un objeto.
    console.log(persona.edad);
    console.log(persona.hogar.ciudad);
    console.log(persona.nacimiento());

    const persona2 = {
        nombre: 'miguel',
        apellido: 'Martinez',
        profesion: 'Diseñador gráfico',
        email: 'correo@correo.com',
        edad: 30,
        musica: ['Trance','Rock','Grunge'],
        hogar: {
            ciudad: 'Guafalajara',
            pais:'Mexico',
        },
        nacimiento: function() {
            return new Date().getFullYear() - this.edad;//this sirve para acceder sus mismas propiedades.
        }
    };

    console.log(persona2.nacimiento());
    console.log(' ');
    
//Video 27 - Creando arreglos de Objetos

    //Arreglo de Objetos
    const autos = [
        {modelo: 'Mustang', motor: 6.0},
        {modelo: 'Camaro', motor: 6.1},
        {modelo: 'Challenger', motor: 6.3},
    ]

    console.log(autos);
    console.log(autos[0].modelo);

    for(let i = 0; i < autos.length; i++){
        //console.log(autos[i].modelo);
        console.log(`${autos[i].modelo} ${autos[i].motor}`);
    }

    autos[0].modelo = 'Audi'

    console.log(autos);

    //NOTA: si se puede modificar los valores de un objeto aunque este declarado con un const.

    console.log(' ');

//Video 28, 29 y 30 - Funciones

    //console.log('Hola');
    //prompt('Cuantos años tienes?');
    //alert("Error");

    //Function declaration
        function saludar(nombre) {
            console.log(`Hola ${nombre}`);
        }
        saludar('Alma');
        saludar('Pablo');
        saludar('Carolina');

        function suma1(a,b) {
            console.log(a+b);
        }
        suma1(1,2);
        suma1(3,4);

        function sumar2(a,b) {
            return a + b;
        }
        let sumas;
        sumas = sumar2(5,2);
        console.log(sumas);
        //NOTA: Es obligatorio que se introduzcan los dos parametros que requiere la funcion, de lo contrario dara error NaN.

        function saludar2(nombre) {
            if(typeof nombre === 'undefined') {nombre = 'Visitante'}//Esto se hace para darle un valor por defecto a la función si no se le asigna uno en el parametro (Forma vieja de trabajar).
            return `Hola ${nombre}`;
        } 
        let saludo;
        saludo = saludar2('Jose');//Con parametro
        console.log(saludo);
        saludo = saludar2(); //Sin parametro
        console.log(saludo);

        //Forma moderna de colocar valores por defectos a una función que no se le a asignado un parametro
        function saludar3(nombre = 'Visitante') {
            return `Hola ${nombre}`;
        } 
        saludo = saludar3('Jose');//Con parametro
        console.log(saludo);
        saludo = saludar3(); //Sin parametro
        console.log(saludo);

    //Function expression
        const suma = function(a = 3,b = 4) {
            return a + b;
        }
        console.log(suma(1,2));
        console.log(suma(5));

        const saludar4 = function(nombre = 'visitante',edad = 20, trabajo = 'desarrollador web') {
            return `Hola, tienes ${edad}, profesion ${trabajo} y te llamas ${nombre}`;
        }
        console.log(saludar4('Juan'));
        console.log(saludar4());


    //Funciones IIFE

    (function(tecnologia) {
        console.log(`Apendriendo ${tecnologia}`);
    })('JavaScript');

    //NOTA: Permite crear la función y llamarla automaticamente con los ultimos ().

    //Métodos de propiedad
    //Cuando una función se pone dentro de un objeto

    const musica = {
        reproducir: function(id) {
            console.log(`Reproduciendo Canción id ${id}`);
        },
        pausar: function() {
            console.log(`Pause a la música`);
        }
    }
    //Los metodos tambien pueden guardarse / crearse fuera del objeto
    musica.borrar = function(id) {
        console.log(`Borrando la canción con el ID: ${id}`);
    }
    
    musica.reproducir(30);
    musica.pausar();
    musica.borrar(2);

//Video 31 - Manejando los errores con Try Catch
    obtenerClientes();

    function obtenerClientes() {
        console.log('Descargando...');
        
        setTimeout(function() {
            console.log('Completo');
        },3000);
    }

    //Función que no existe
    try{
        algo();
    } catch(error) {
        console.log(error);
    } finally {
        console.log('No le importa, ejecuta de todos modos');
    }

//Video32 - Fechas

    const diaHoy = new Date();
    console.log(diaHoy);

    //Fecha en Especifico Mas,Día y año
    let navidad2017 = new Date('12-25-2017');
    console.log(navidad2017);

    let vals;
    //Mes
        vals = diaHoy.getMonth(); //Regresa el mes pero con un menos ya que empieza a contar desde el 0 los meses (0 a 11)
    //Días
        vals = diaHoy.getDate();
        vals = diaHoy.getDay();//NO regresa el día ques por una extraña razón.
    //Año
        vals = diaHoy.getFullYear();
    //Minutos
        vals = diaHoy.getMinutes();
    //Hora
        vals = diaHoy.getHours();
    //Segundos
        vals = diaHoy.getSeconds();
    //Milisegundos que se han registrado desde 1970
        vals = diaHoy.getTime();
    //Para modificar o agregar los valores se usa set (Se puedo usar set en las funciones anteriores)
        vals = diaHoy.setFullYear(2016);
        vals = diaHoy.getFullYear();


    console.log(vals);

//Video 33, 34 y 35 - Estucturas de control. if, else, elseif

    const edads = 19;

    if(edads >= 18){
        console.log('Si puedes entrar al sitio');
    } else {
        console.log('No puedes entrar al sitio');
    }

    //Comprobar que una variable tiene un valor
    const puntaje = 1000;

    if(puntaje){
        console.log(`El puntaje fue de ${puntaje}`);
    } else {
        console.log('No hay puntaje');
    }

    //practica

    let efectivo = 500;
    let totalCarrito = 300;

    if(efectivo > totalCarrito){
        console.log('Pago Correcto');
    } else{
        console.log('Fondos Insuficientes');
    }

    //elseid con operador &&
    let hora = 24;
    if (hora > 0 && hora <= 10) {
        console.log('Buenos días!');
    } else if(hora > 10 && hora <= 18) {
        console.log('Buenas tardes!');
    } else if (hora >18 && hora <= 24) {
        console.log('Buenas Noches!');
    } else {
        console.log('Hora no valida!');
    }

    let puntajes = 100;
    if (puntajes < 150) {
        console.log('Puntaje < 150');
    } else if(puntajes < 250) {
        console.log('Puntaje < 250');
    } 

    //Operador ||
    let efect = 300, 
        credit = 700,
        disponible = efect + credit,
        totalCarritos = 500;
    if (totalCarritos < efect  ||  totalCarritos < credit) {
        console.log('Puedo pagar');
    } else if (totalCarritos < disponible){
        console.log('Pague con ambos');
    }
    else{
        console.log('No Puedo pagar');
    }

    //Ternario - If y else en una sola linea

    const loguedo = true;

    console.log( loguedo === true ? 'Si se logueo' : 'No se logueo');
                // Condición del if ?(Si se cumple la condición) :(Si no se cumple la condición).

//Video 36 - Los Switch
    const metodoPago = 'Efectivo';
    switch(metodoPago) {
        case 'Efectivo':
            console.log(`El usuario pago con ${metodoPago}`);
            break;
        case 'Cheque':
            console.log(`El usuario pago con ${metodoPago}`);
            break;
        case 'Tarjeta':
            console.log(`El usuario pago con ${metodoPago}`);
            break;
        default:
            console.log('Método de pago no soportado');
            break;
    }

    //Ejemplo de los meses
    let mes;
    switch(new Date().getMonth()) {
        case 0:
            mes = 'Enero';
            break;
        case 1:
            mes = 'Febrero';
            break;
        case 2:
            mes = 'Marzo';
            break;
        case 3:
            mes = 'Abril';
            break;
        case 4:
            mes = 'Mayo';
            break;
        case 5:
            mes = 'Junio';
            break;
        case 6:
            mes = 'Julio';
            break;
        case 7:
            mes = 'Agosto';
            break;
        case 8:
            mes = 'Septiembre';
            break;
        case 9:
            mes = 'Octubre';
            break;
        case 10:
            mes = 'Noviembre';
            break;
        case 11:
            mes = 'Diciembre';
            break;
    }

    console.log(`Este mes es ${mes}`);

//Video 37 - Iteraciones -- For loops
    
    //For loop
    for (let i = 0; i < 10; i++) {
        if (i == 5) {
            console.log('Voy en el 5');
            continue; //Sirve para saltar la información si se cumple la condición.
        }
        console.log(`${i}`);
    }

    //Como saber que un número es par o impar
    for (let i = 0; i < 10; i++) {
        if (i % 2 == 0) {
            console.log(`El número ${i} es Par`);
        } else{
            console.log(`El número ${i} es Impar`);
        }
    }

    const arregloProductos = ['Camisa','Boleto','Guitarra','Disco'];
    
    for (let i = 0; i < arregloProductos.length; i++) {
        console.log(`Tu producto ${arregloProductos[i]} fue agregado`);
        
    }
    
//Video 38 - Iteraciones -- While y Do while

    //While - Se ejecuta hasta que se cumpla la condición que se le especifica (Validad primero la condición para iniciar su ejecución).
        let i = 0;
        while(i < 10) {
            console.log(`Número ${i}`);
            i++;
        }

        //While con arreglos
        const musicas = ['Canción 1','Canción 2','Canción 3'];
        i = 0;
        while(i < musicas.length) {
            console.log(`Reproduciendo la ${musicas[i]}`);
            i++;
        }
    
    //Do While - Se ejecuta al menos una vez independientemente la condición, luego es que chequea la condición.
        i = 0;
        do {
            console.log(`Número ${i}`);
            i++;
        } while(i < 10);

    console.log(' ');

//Video 39 - Iteraciones -- ForEach

    //Recorrer un arreglo con forEach

    const pendientes = ['Tarea','Comer','Proyecto','Aprender JavaScript'];

    pendientes.forEach(function(pendientes, index) {
        console.log(`${index} : ${pendientes}`);
    });

    //Map para recorrer un arreglo de objetos

    const carritos_3 = [
        {id: 1, producto: 'Libro'},
        {id: 2, producto: 'Camisa'},
        {id: 3, producto: 'Guitarra'},
        {id: 4, producto: 'Disco'},
    ];
    const nombreProductos = carritos_3.map(function(carritos_3){
        return carritos_3.producto;
    })
    console.log(nombreProductos)

    //Otro ejemplo
    const automovils = {
        modelo: 'Camaro',
        motor: 6.1,
        anio: 1969,
        marca: 'Chevrolet'
    }
    for (let auto in automovils) {
        console.log(`${auto} : ${automovils[auto]}`); //Permite devolver la llave de cada uno de los valores del objeto
    }
    console.log(automovils)

//Video 40 - Windows Object

    /*
        Solo en consolo de chrome

        window - Permite ver información del usuario en la consola de chrome
    
    */

    //confirm - pantalla de confirmación

    /*if (confirm('Eliminar?')) {
        console.log('Eliminado');
    } else {
        console.log('Nada paso');
    }*/

    //Saber cuanto mide elnavegador web completo
    let altura,anchura;

    altura = window.outerHeight;
    anchura = window.outerWidth;
    console.log(`${altura}`);
    console.log(`${anchura}`);

    //Saber cuanto mide la vista de la pagina web mostrada por el navegador
    altura = window.innerHeight;
    anchura = window.innerWidth;
    console.log(altura);
    console.log(anchura);

    //Ubicación
    let ubicacion;
    ubicacion = window.location; //Te trae la información de la navegación.

    ubicacion = window.location.search; //Si se hace una busqueda en la url la devuelve aquí.

    //window.location.href = 'http://twitter.com' //Permite redireccionar a otra pagina web.

    //window.history.go(-3) //Te redirecciona a las paginas que hallas navegado anteriormente.

    //Navegator
    ubicacion = window.navigator; //Información del navegador.
    ubicacion = window.navigator.appName; //Nombre de la app del navegador.
    ubicacion = window.navigator.appVersion; //Devuelve la versión del navegador.
    ubicacion = window.navigator.language; //Te devuelve el idioma en el que esta configurado el navegador.


    console.log(ubicacion);
    console.log(' ');

//Video 41 - Scope
    //Estas son variables globales
    var a = 'a';
    let b = 'b';
    const c = 'c';

    //Scope de la función
    function funcionScope(){
        var a = 'A';
        let b = 'B';
        const c = 'C';
        console.log('FUNCION: ' + a,b,c);
    }
    funcionScope();

    //NOTA: Las variables de las funciones son locales las cuales no afectan a las variables globales, solo dentro de la misma función.

    //Scope de Bloque {}
    if(true) {
        var a = 'AA';
        let b = 'BB';
        const c = 'CC';
        console.log('BLOQUE: ' + a,b,c);
    }

    //NOTA: Para este caso, las variables declaradas con "var" si reescribe a la variable global, a diferencia de las variables declaradas con 'const' y 'let', no se veran afectafas a las variables globales, ya que son unicas dentro de los {}

    //For
    for (let a = 0; a < 10; a++) {
        console.log(a);
    }

    //NOTA: Para los for se recomienda utilizar las variables con let, para evitar afectar los valores de las mismas declarandolas con var.

    console.log('GLOBALES: '+ a,b,c);


/*-----------------------------------------------------------*/

///CARPETA DOM, Window y Scripting///

//Video 43s DOM y Scripting

    
    