// Eliminar de Local Storage
localStorage.clear();


//Video 43 - DOM Y SCRIPTS
  /*  let elemento;

    //Permite extraer la información de las etiquetas del HTML
    elemento = document;
    elemento = document.all; //Las extrae como arreglos
    elemento = document.all[10];
    elemento = document.head;
    elemento = document.body;
    elemento = document.domain; //Extrae el dominio de la web
    elemento = document.URL;
    elemento = document.characterSet;
    elemento = document.forms;
    elemento = document.forms[0].id; //Para acceder a los id del formulario
    elemento = document.forms[0].className; //Para acceder al nombre de la clase del formulario.
    elemento = document.forms[0].classList; //Para acceder a los nombres de las clases del formulario en listas.
    elemento = document.forms[0].classList[0];
    elemento = document.images; //Trae toda la información del as imagenes en un objeto 
    elemento = document.images[2].src;
    elemento = document.images[2].getAttribute('src'); //Ruta personalizada

    console.log(elemento);

    //Scripts
    elemento = document.scripts;

    elemento = document.images;

    let imagenes = document.images;
    let imagenesArr = Array.from(imagenes);

    imagenesArr.forEach(function(imagen){
        console.log(imagen);
    })

    console.log(imagenesArr);
   */

//Video 44 -
 /*
    //getElementById
    let elemento;

    elemento = document.getElementById('hero').className;
    elemento = document.getElementById('hero').id;
    
    console.log(elemento)

    let encabezado;

    encabezado = document.getElementById('encabezado').className;
    encabezado = document.getElementById('encabezado').textContent;//Obtener el texto de la etiqueta
    encabezado = document.getElementById('encabezado').innerText;  // Otra forma de obtener el texto de la etiqueta
    encabezado = document.getElementById('encabezado')
    encabezado.style.background = '#333'; //Permite hacerle modificaciones al estilo de la pagina
    encabezado.style.color = '#fff';
    encabezado.style.padding = '20px';

    // Cabiar texto

    encabezado.textContent = 'Los mejores cursos';
    encabezado.innerText = 'Los mejores cursos'; //Sirve para reescribir el texto igualandolo y solo te trae la información que contiene.
    
    console.log(encabezado);

    //NOTA: Con esta función getElementById() solo se puede extraer ID 
 */

//Video 45 - Selección de un elemento
    /*
    //Query Selector
    const encabezado = document.querySelector('#encabezado');

    // Aplicar CSS
    encabezado.style.background = '#333'; //Permite hacerle modificaciones al estilo de la pagina
    encabezado.style.color = '#fff';
    encabezado.style.padding = '20px';
    encabezado.textContent = 'Los mejores cursos';
    
    console.log(encabezado);

    const encabezado_1 = document.querySelectorAll('.enlace');
    console.log(encabezado_1);

    const encabezado_2 = document.querySelector('.card img')
    console.log(encabezado_2);

    let enlace;

    enlace = document.querySelector('#principal a:first-child');//Primer hijo
    enlace = document.querySelector('#principal a:nth-child(3)');//Slección de uno de los hijos
    enlace = document.querySelector('#principal a:last-child');//Ultimo hijo
    console.log(enlace);
    

    //NOTA: Trabaja igual que getElementById() la diferencia es que se puede extrar tanto ID como CLASES y deben ser especificadas (#encabezado o .encabezado), tambien permite seleccionar las etiquetas de html, tambien se puede desglosar la busqueda utilizando tanto clases, id y etiquetas html(así como jquery o css3).

    //NOTA: Permite hacer busquedas aplicando propiedades de CSS3.

    //NOTA: Si se selecciona una clase que se usa varias veces va a extraer por defecto el primero que consigue ya que solo trae un valor, para traerlos todo se debe colocar .querySelector.All('.encabezado').   

    */
//Video 46 - Selección de múltiples elementos p1
    /*
    let enlaces = document.getElementsByClassName('enlace')[3]; //Permite extraer data de una clase y extrae todos los que existan o especificando se puede traer el que se quiera.

    enlaces = enlaces[0];
    console.log(enlace);

    const listaEnlaces = document.querySelector('#principal').getElementsByClassName('enlace');
    console.log(enlace); //Se puede realizar varia combinaciones de lo visto anteriormente.


    const links = document.getElementsByTagName('a');//Permite extraer infromación de una etiqute html.
    links[18].style.color = 'red';
    links[18].textContent = "Nuevo enlace";
    console.log(links);
    let enlaces_1 = Array.from(links);

    enlaces_1.forEach(function(enlace){
      console.log(enlace.textContent);
    });

    //NOTA: Tambien se puede aplicar los elemento de css anteriormente explicados.

//Video 47 - Selección de múltiples elementos p2

    const enlaces_2 = document.querySelectorAll('#principal .enlace');
    enlaces_2[1].style.background = 'red';
    enlaces_2[1].textContent = 'nuevo enlaces';

    console.log(enlaces_2);

    const enlaces_3 = document.querySelectorAll('#principal a:nth-child(odd)'); //Extrae la data de los impares.

    enlaces_3.forEach(function(impares){
      impares.style.backgroundColor = 'blue';
    });

    console.log(enlaces_3);
    //NOTA: Te devuelve una lista de todos los resultados encontrados.
    */
//Video 48, 49 - Traversing
    /*
    //Padre al hijo
      const navegacion = document.querySelector('#principal');
      console.log(navegacion.childNodes); //Devuleve la información de los enlaces pero con data de más devido a los espacios entre la etiquetas html, devuelve los nodos los cuales son las etiquetas del html.

      console.log(navegacion.children);//Con esta si devuelve los hijos sin contar los espación en blanco que los separan en el html.

      console.log(navegacion.children[0].nodeName);//devuelve el nombre del nodo.

      console.log(navegacion.children[0].nodeType);
      // 1 = Elementos html
      // 2 = Atributos
      // 3 = Text node 
      // 8 = comentarios
      // 9 = documentos
      // 10 = doctype

      //NOTA: children se puede usara tantas veces sea necesario siempre y cuando la etiqueta tenga información.

      const cursos = document.querySelectorAll('.card');

      console.log(cursos[0].lastElementChild);//Ultimo elemento de la carta.
      console.log(cursos[0].firstElementChild);//El primer elemento de la carta.
      console.log(cursos[0].childElementCount);//Se encarga de contar la cantidad de elemento con el que cuenta la carta.

      //NOTA: De esta manera se hace traversing del padre al hijo.

    //Hijo al padre
      const enlaces_4 = document.querySelectorAll('.enlace');


      console.log(enlaces_4[0].parentElement); //Con esto se trae la información de la etiqueta html padre.

      //NOTA: El 'parentElement' Se puede aplicar tantas veces sea necesario.

      console.log(enlaces_4[4].previousSibling);//Trae el elemento anterior al que contiene la variable, pero contando los espacios.

      console.log(enlaces_4[4].previousElementSibling);//Trae el elemento anterior al que contiene la variable.

      console.log(enlaces_4[0].nextElementSibling);//Trae el elemento anterior al que contiene la variable.
      */
//Video 50 - Creando elementos
    /*
    //Crear enlaces
    const enlace = document.createElement('a');

    //Agregamos un clase 
    enlace.className = 'enlace';
    //añadir id
    enlace.id = 'nuevo-id';
    // atribute href
    enlace.setAttribute('href','#');
    // Añadir texto
    enlace.appendChild(document.createTextNode('Nuevo enlace')); 
    enlace.textcontent = 'Nuevo Enlace'; //Estna estas dos forma de agregar texto.

    //agregarlo al html

    document.querySelector('#secundaria').appendChild(enlace);
    */
//Video 51 - Reemplazar elementos
    /*
    //Reemplazar elementos 
    const nuevoEncabezado = document.createElement('h2');

    //agregar id
    nuevoEncabezado.id = 'encabezado';
    // agregar nuevo texto 
    nuevoEncabezado.appendChild(document.createTextNode('Los mejores Cursos'));

    // Elemento anterior (-sera reemplazado)
    const anterior = document.querySelector('#encabezado');

    const elPadre = document.querySelector('#lista-cursos');

    elPadre.replaceChild(nuevoEncabezado, anterior); // PErmite reemplazar etiquetas, para ello se necesita crear etiqueta nueva y extraer la que se quiere reemplazar ya que deben pasar por los propiedades de la función (nuevo, anterior):

    console.log(anterior.parentElement);
    */
//Video 52 - Eliminar elementos
    /*
    const enlaces = document.querySelectorAll('.enlace');
    const navegacion = document.querySelector('#principal');

    enlaces[0].remove();//Permite revomer el elemento seleccionado.

    navegacion.removeChild(enlaces[1]);// Otra forma de eleminar elementos con la herencia de los hijos.
    console.log(enlaces);
    
    const primeLi = document.querySelector('.enlace');

    let elemento;

    //obtener una clase de CCS 
    elemento = primeLi.className;//devuelve el nombre.
    elemento = primeLi.classList.add('nueva-clase');//Perimite agregarle nuevs clases.
    elemento = primeLi.classList.remove('nueva-clase');//Permite eliminar las clases.
    elemento = primeLi.classList;//devuelve en una lista.

    // Leer atributos
    elemento = primeLi.getAttribute('href');// Agregar atributos.
    primeLi.setAttribute('href', 'http://facebook.com');// Modifica el atributo.
    primeLi.setAttribute('data-id', 20);
    elemento = primeLi.hasAttribute('data-id');// Comprueba si contiene ese atributo con True o False.
    primeLi.removeAttribute('data-id');// Permite eliminar los atributos de las etiquetas html

    elemento = primeLi
    console.log(elemento);

    //NOTA: No solo puede ser eliminado atributos, sino clases, id e etiquetas

    */

//// CARPETA EVENT LISTENERS

//Video 53 - Event Listener Click

    //EventListener click al buscador
    /*
    document.querySelector('#submit-buscador').addEventListener('click', function(e){//Permite agregarle un acción al buscador cuando es pulsado con el click.
      e.preventDefault();//Permite que no se ejecute la acción de la redirección del buscador.
      alert('Buscand o Cursos');
    });
    */
    //Otro forma de hacerlo 
    /*
    document.querySelector('#submit-buscador').addEventListener('click', ejecutarBoton);

    function ejecutarBoton(e) {
      e.preventDefault();
      let elemento;
      elemento = e;
      elemento = e.target;

      console.log(elemento);
    }

    //NOTA: Algunas funciones pueden ser llamadas sin los ().
    

    document.querySelector('#encabezado').addEventListener('click',function(e){
      
      e.target.innerText = 'Nuevo Encabezado';
      console.log();
    });
    */

//Video 54 - Event Listener Mouse
    /*
    // Variables 
    const encabezado = document.querySelector('#encabezado');
    const enlaces = document.querySelectorAll('.enlace');
    const boton = document.querySelector('#vaciar-carrito');
    */
    // Click
    //boton.addEventListener('click', obtenerEvento);
    
    // Doble Click
    //boton.addEventListener('dblclick', obtenerEvento);
    
    // Mouse Enter - Se activa al colocar el cursor encima del boton
    //boton.addEventListener('mouseenter', obtenerEvento);
    
    // Mouse leave - Se activa cuando se quita el cursor del boton
    //boton.addEventListener('mouseleave', obtenerEvento);
    
    // Mouse Over - Se activa al colocar el cursor encima del boton
    //boton.addEventListener('mouseover', obtenerEvento);

    // Mouse Out - Similar a mouse leave
    //boton.addEventListener('mouseout', obtenerEvento);

    // Mouse down - Se activa la dar click al boton
    //boton.addEventListener('mousedown', obtenerEvento);

    // Mouse Up - Se activa al soltar el click del boton
    //boton.addEventListener('mouseup', obtenerEvento);

    // Mouse move - Se activa al movilizarse por encima de la etiqueta html o boton.
    //encabezado.addEventListener('mousemove', obtenerEvento);

    /*
    function obtenerEvento(e) {
      console.log(`EVENTO: ${e.type}`);
    }*/

//Video 55 - Eventos para los inputs

    //const busqueda = document.querySelector('#buscador');

    // Este evento se activa cuando se escribe en la barra del buscador
    //busqueda.addEventListener('keydown', obtenerEvento);

    // Este evento se activa cuando se suelta la tecla que se pulsa en la barra del buscador
    //busqueda.addEventListener('keyup', obtenerEvento);

    // Este evento se activa cada vez que escribe en la barra del buscador
    //busqueda.addEventListener('keypress', obtenerEvento);

    // Este evento se activa cuando se selecciona la barra del buscador 
    //busqueda.addEventListener('focus', obtenerEvento);

    // Este evento se activa al dar click fuera de la barra del buscador (Este evento es muy utilizado para validar caracteres escritos en un campo entre otras cosas...) 
    //busqueda.addEventListener('blur', obtenerEvento);

    // Este evento se activa cuando se corta un texto, en este caso el texto de la barra de busqueda
    //busqueda.addEventListener('cut', obtenerEvento);

    // Este evento se activa cuando se copia un texto, en este caso el texto de la barra de busqueda
    //busqueda.addEventListener('copy', obtenerEvento);

    // Este evento se activa cuando se copia un texto, en este caso el texto de la barra de busqueda
    //busqueda.addEventListener('paste', obtenerEvento);

    // Este evento se activa cuando se detecta alguna acción de los eventos anteriormente usados (No funciona como lo describen en el curso, solo se activa al escribbir texto en la barra del buscador )
    //busqueda.addEventListener('input', obtenerEvento);

    // Este evento se activa en los campos Select
    //busqueda.addEventListener('change', obtenerEvento);

    /*
    function obtenerEvento(e) {
      document.querySelector('#encabezado').innerText = busqueda.value
      console.log(busqueda.value);
      console.log(`EVENTO: ${e.type}`);
    }*/

//Video 56 - Event Bubbling (Burbujas de eventos)
    /*
    const card = document.querySelector('.card');
    const infoCurso = document.querySelector('.info-card');
    const agregarCarrito = document.querySelector('.agregar-carrito'); 

    card.addEventListener('click', function(e) {
      console.log('Click en Card');
      e.stopPropagation();
    });

    infoCurso.addEventListener('click', function(e) {
      console.log('Click en Info Curso');
      e.stopPropagation();
    });

    agregarCarrito.addEventListener('click', function(e) {
      console.log('Click en Agregar a Carrito');
      e.stopPropagation(); // Con esta función permite que no se propague la acciones y se ejecute solo una por individual.
    });

    //NOTA: Permite realizar varias acciones a la vez, ya que cada una de las clases o los id que estes entrelazados como es este caso.

    */

//Video 57 - Delegation

    /*
    document.body.addEventListener('click', eliminarElemento);

    function eliminarElemento(e){
      e.preventDefault();

      //console.log('Click');
      //console.log(e.target.classList);

      // Esto permite verificar si se selecciono el botón de borrar-curso o se selecciono otras clases
      if(e.target.classList.contains('borrar-curso')){
        console.log('Si!');
      } else {
        console.log('No!');
      }
      
      if(e.target.classList.contains('borrar-curso')){
        console.log(e.target.parentElement.parentElement.remove());
      } //Esta condición agrega la funcionabilidad de borrar los cursos que han sido agregado en el carrito de compra (Permite determinar las etiquetas html que agregan la vista de los productos seleccionados ).

      if(e.target.classList.contains('agregar-carrito')){
        console.log('Curso agregado');
      }

    };
    */


//// CARPETA LOCAL STORAGE y SESSION STOREGE

//Video 58 - Agregando, leyendo, borrando y limpiando el LocalStorage

    // Agregar a Local Storage - Permite almacenar información en el navegador web permitiendo acceder a ella cada vez que se ingrese a la pagina web (Dicha información se borra si el usuario decide borrar la memoria caché del navegador).
    localStorage.setItem('nombre', 'Juan');

    // Session Storege - Permite guardar infomación en el navegador, adiferencia del local storage esta información se borra cuando se cierra la pagina.
    sessionStorage.setItem('nombre', 'Juan');

    // Eliminar de Local Storage - Permite eliminar data del Local Storage.
    //localStorage.removeItem('nombre');

    localStorage.setItem('nombre', 'Juan');
    const nombre = localStorage.getItem('nombre'); // Extraes información del Local storage con el nombre de la llave.
    console.log(nombre);

    // Eliminar todo el Local Storage
    localStorage.clear();

    //NOTA: El Local Storage permite almacenar puros string.