/**
 * Arreglos unidimensional -> Vectores
 * 
 * **************************************
 *  Ejercicio: Crear un vecto de cinco posiciones, posteriormente guardar un número en cada una de las posiciones del vector,
 *             y finalmente imprimir en pantalla cada una de las posiciones para verificar que se hayan guardado los
 *             números de manera correcta.
 */
public class ArregloVector {
    public static void main(String args[]) {
        int num[] = new int[5];

        num[0] = 5;
        num[1] = 220;
        num[2] = 8;
        num[3] = 458;
        num[4] = 22;

        System.out.print("[" + num[0] + "]");
        System.out.print("[" + num[1] + "]");
        System.out.print("[" + num[2] + "]");
        System.out.print("[" + num[3] + "]");
        System.out.print("[" + num[4] + "]");
    }
    
}