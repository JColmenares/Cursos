import javax.swing.*;
/**
 * FormularioSwing2
 * 
 *  //NOTA: El constructor debe llamarse igual que la clase.
 * 
 */
public class FormularioSwing2 extends JFrame{
    
    public FormularioSwing2(){
        setLayout(null);
    }
    
    public static void main(String args[]) {
        FormularioSwing2 formulario1 = new FormularioSwing2(); //Objeto del constructor
        formulario1.setBounds(0,0,400,550); //Identifica la coordenada de la interfaz (los lados, altura, ancho_interfaz, _interfaz);
        formulario1.setVisible(true); //Para visualizar la interfaz.  
        formulario1.setLocationRelativeTo(null); //Con esto permirte que siempre el programa se ejecute en el centro de la pantalla, sin importar las coordenada de inicio que se colocaron.  
        formulario1.setResizable(false); //Permite (true) o niega (false) el permiso de que el usuario modifique el tamaño de la interfaz inicial al ejecutarse.
    }
}