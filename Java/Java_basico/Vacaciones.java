import java.util.Scanner;
public class Vacaciones {
    public static void main(String args[]){
        
        Scanner in = new Scanner(System.in,"UTF-8"); //En caso de ser necesario se le coloca el siguiente argumento: "UTF-8"
        String nombre = "";
        int clave = 0;
        int antiguedad = 0;

        System.out.println("******************************************************");
        System.out.println("*Bienvenido al sisema vacaciones de coca-cola Company*");
        System.out.println("******************************************************");
        System.out.println("");

        System.out.println("¿Cuál es el nombre del trabajador? ");
        nombre = in.nextLine();
        System.out.println("");

        System.out.println("¿Cuantos años de servicio tiene el trabajador? ");
        antiguedad = in.nextInt();
        System.out.println("");

        System.out.println("¿Cuál es la clave del departamento?");
        clave = in.nextInt();
        System.out.println("");

        if (clave == 1) {
            if (antiguedad == 1) {
                System.out.println("El trabajador " + nombre + ", derecho 6 días de vacaciones.");
            } else if (antiguedad >= 2 && antiguedad <= 6) {
                System.out.println("El trabajador " + nombre + ", derecho 14 días de vacaciones.");
            } else if (antiguedad >= 7) {
                System.out.println("El trabajador " + nombre + ", derecho 20 días de vacaciones.");
            } else {
                System.out.println("El trabajador " + nombre + ", aun no tiene derecho a vacaciones.");
            }
        } else if (clave == 2) {
            if (antiguedad == 1) {
                System.out.println("El trabajador " + nombre + ", derecho 7 días de vacaciones.");
            } else if (antiguedad >= 2 && antiguedad <= 6) {
                System.out.println("El trabajador " + nombre + ", derecho 5 días de vacaciones.");
            } else if (antiguedad >= 7) {
                System.out.println("El trabajador " + nombre + ", derecho 22 días de vacaciones.");
            } else {
                System.out.println("El trabajador " + nombre + ", aun no tiene derecho a vacaciones.");
            }
        } else if (clave == 3) {
            if (antiguedad == 1) {
                System.out.println("El trabajador " + nombre + ", derecho 10 días de vacaciones.");
            } else if (antiguedad >= 2 && antiguedad <= 6) {
                System.out.println("El trabajador " + nombre + ", derecho 20 días de vacaciones.");
            } else if (antiguedad >= 7) {
                System.out.println("El trabajador " + nombre + ", derecho 30 días de vacaciones.");
            } else {
                System.out.println("El trabajador " + nombre + ", aun no tiene derecho a vacaciones.");
            }
        } else {
            System.out.println("La clave seleccionada no se encuentra disponible.");
        }
    }
}