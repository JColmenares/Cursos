import java.util.Scanner;

/**
 * Login
 */
public class Login {
    public static void main(String args[]) {
        String usuario = "", password = "";
        Scanner entrada = new Scanner(System.in);

        System.out.print("Ingresa tu nombre de usuario: ");
        usuario = entrada.nextLine();

        System.out.print("Ingresa tu contraseña: ");
        password = entrada.nextLine();

        if (usuario.equals("jose") && password.equals("123456")) {
            System.out.print("Bienvenido " + usuario);
        } else {
            System.out.print("Nombre de usuario o contraseña incorrectos");
        }
    }
}