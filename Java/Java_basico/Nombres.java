import java.util.Scanner;

public class Nombres {
    public static void main(String args[]) {
        String nombreUno = "",nombreDos = "";
        Scanner entrada = new Scanner(System.in);

        System.out.print("Por favor ingrese el primer nombre: ");
        nombreUno = entrada.nextLine();

        System.out.print("Por favor ingrese el segundo nombre: ");
        nombreDos = entrada.nextLine();

        if (nombreUno.equalsIgnoreCase(nombreDos)) { //Permite realizar conmparación con otra cadena de caracteres (String) (Con equals: Verifica aunq tenga mayusculas, equalsIgnoreCase: ingnora las mayusculas o munusculas y las comparan si son igual).
            System.out.println("Los nombres son iguales");
        } else {
            System.out.println("Los nombres son diferentes");    
        }
    }
    
}