import javax.swing.*;
/**
 * FormularioSwing
 * 
 * //NOTA: private -> Se usa para identificar que solo esa variable puede ser usada en dicha clase donde se declara.
 */
public class FormularioSwing extends JFrame{ //Herencia de esta clase para la parte grafica
    int numUno = 0;
    private JLabel label1; //Permite crear etiquetas, muestra datos en la parte grafica sin usar System.out.print()

    public FormularioSwing(){ //Se crea el CONSTRUTOR el cual tendra todo el diseño.
        setLayout(null); //PErmite indicar al programa por medio de coordenadas donde se quieren colocar los elementos en el programa. 
        label1 = new JLabel("La Geekipedia de Jose"); // Un objeto
        label1.setBounds(10, 20, 200, 300); //Se especifica las cordenadas donde se va a dibujar con eje x, eje y, Ancho y alto. 
        add(label1); //Agrega lo anterior a la varible
    }

    public static void main(String args[]){
        FormularioSwing formulario1 = new FormularioSwing(); //Se crea un objeto de l contructor que creamos para para poder hacer el llamado del diseño y así usar JFrame
        formulario1.setBounds(0,0,400,300); //Con esto se identifica el tamaño de la ventana y que se vea exactamente en el centro.
        formulario1.setVisible(true); //Permite identificar si la pantalla se va a ver (true) u ocultar (false).
        formulario1.setLocationRelativeTo(null); //Permite que cuando se ejecute aparezca en el centro de la pantall.
    }
        
}