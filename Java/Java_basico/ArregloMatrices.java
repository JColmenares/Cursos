/**
 * Arreglos Bidimencional - Matrices
 * 
 * ************************************************************
 * 
 *  Ejercicios: Crear un programa que muestre en pantalla una matriz 
 *              de 2 filas y 2 columnas con un número dentro de cada posición tal 
 *              y como se muestra a continuación.
 * 
 *              [5] [2]
 *              [2] [5]
 * 
 */
public class ArregloMatrices {
    public static void main(String args[]) {
        int num[][] = new int[2][2];

        num[0][0] = 5;
        num[0][1] = 2;
        num[1][0] = 5;
        num[1][1] = 2;

        System.out.print("[" + num[0][0] + "]");
        System.out.println("[" + num[0][1] + "]");
        System.out.print("[" + num[1][0] + "]");
        System.out.println("[" + num[1][1] + "]");
        
    }
    
}