public class Operaciones_2 {
    public static void main(String args[]) {
        int operacion = 4;
        int num_uno = 8;
        int num_dos = 4;
        int resultado = 0;

        if (operacion == 1) {
            resultado = num_uno + num_dos;
            System.out.println("El resultado de la Suma es: " + resultado);
        } else if(operacion == 2) {
            resultado = num_uno - num_dos;
            System.out.println("El resultado de la Resta es: " + resultado);
        } else if(operacion == 3) {
            resultado = num_uno * num_dos;
            System.out.println("El resultado de la Multiplicación es: " + resultado);
        } else if(operacion == 4) {
            resultado = num_uno / num_dos;
            System.out.println("El resultado de la División es: " + resultado);
        } else {
            System.out.println("La operación Seleccionada no existe.");
        }
    }
    
}