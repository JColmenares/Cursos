import javax.swing.*;

/**
 * SwingJLabel
 */
public class SwingJLabel extends JFrame {

    private JLabel label1; //No es necesario inicializarla
    private JLabel label2;

    public SwingJLabel() { //Constructor
        setLayout(null);
        label1 = new JLabel("Interfaz gráfica.");
        label1.setBounds(10,20,300,30);
        add(label1);
        label2 = new JLabel("Versión 1.0");
        label2.setBounds(10,100,100,30);
        add(label2);
    }
    public static void main(String args[]) {
        SwingJLabel formulario1 = new SwingJLabel();
        formulario1.setBounds(0,0,300,200);
        formulario1.setResizable(false);
        formulario1.setVisible(true);
        formulario1.setLocationRelativeTo(null);
    }
}