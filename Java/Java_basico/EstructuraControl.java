public class EstructuraControl {
    public static void main(String args[]){
        int numUno =5, numDos = 3, resultado = 0;
        int parametro = 3;

        //Switch es una estructura de control
        switch(parametro){
            case 1: resultado = numUno + numDos;
                    System.out.println("El resultado de la Suma es: " + resultado);
                    break;
            case 2: resultado = numUno - numDos;
                    System.out.println("El resultado de la Resta es: " + resultado);
                    break;
            case 3: resultado = numUno * numDos;
                    System.out.println("El resultado de la Multiplicación es: " + resultado);
                    break;
            case 4: resultado = numUno / numDos;
                    System.out.println("El resultado de la División es: " + resultado);
                    break;
            default: System.out.println("Error, la opción no existe");
                    break;
        }
    }
}