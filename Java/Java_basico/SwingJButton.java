import javax.swing.*; //Permite importar las funciones para crear la interfaz grafica
import java.awt.event.*; //Permite agrega e importa funcionalidades que podemos darle a los componentes

/**
 * SwingJButton - Funcion para crear boton, se crea la vista del botón y luego se crea la acción del mismo.
 */
public class SwingJButton extends JFrame implements ActionListener { //Permite verificar si se ejecuta un evento en la clase

    JButton boton1;
    public SwingJButton() {
        setLayout(null);
        boton1 = new JButton("Cerrar");
        boton1.setBounds(300,250,100,30);
        add(boton1);
        boton1.addActionListener(this); //Agrega un evento al boton
    }
    
    public void actionPerformed(ActionEvent e) { //Permite capturar el evento del boton, Se crea un espacio en memoria del evento
        if(e.getSource() == boton1){ //Este metodo contiene el objeto donde se origino el evento verifica el boton que se pulso
            System.exit(0); //Permite cerrar la ventana.
        }        
    }

    public static void main(String arg[]) {
        SwingJButton formulario1 = new SwingJButton();
        formulario1.setBounds(0,0,450,350);
        formulario1.setVisible(true);
        formulario1.setResizable(false);
        formulario1.setLocationRelativeTo(null);
    }
}