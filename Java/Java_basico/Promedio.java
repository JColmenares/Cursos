public class Promedio{
    public static void main(String args[]){
        int mate = 5;
        int biolog = 4;
        int quimic = 3;
        int promedio = 0;

        promedio = (mate + biolog + quimic) / 3;

        if (promedio >= 6) {
            System.out.println("El alumno Aprobó " + promedio);
        } else {
            System.out.println("El alumno Reprobó "  + promedio);
        }
    }
}