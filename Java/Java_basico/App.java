public class App{  //El estandar de Java establece que el nombre de las clases deben comenzar con la primera letra en mayuscula
    public static void main(String args[]){  //Metodo main identifica el comienso del programa (Sin él no corre el programa)
        System.out.println("Hola mundo");    //Permite mostrar texto en consola
    }

}

/* NOTA IMPORTANTE: Para ejecutar un archivo java es indispensable que el archivo lleve el mismo nombre de la clase
                    que fue declarada en el mismo.
 */

/*Como ejecutar archivos Java en cosola Windows:

    1) Se debe depurar el codigo para verificar que no tenga errores:
        >javac app.java
    Este generarara un archivo .class si no presento errores.

    2) Luego de copilar el codigo se puede ejecutar el codigo de las siguiente manera:
        >java app
    Se debe colocar el nombre del archivo sin la extención y se ejecutara al instante..
*/