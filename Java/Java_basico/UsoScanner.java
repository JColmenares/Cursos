/**
 * Scanner - Permite al usuario introducir data por consola
 */
import java.util.Scanner;
public class UsoScanner {
    public static void main(String args[]){

        Scanner in = new Scanner(System.in); //PErmite inicializar la varible Scanner. 
        /*IMPORTANTE: Dentro de los parentesis se debe escribir siempre System.in , independientemente
                      como se llame la variables objeto.
        */   
        String nombre = "";
        int numUno = 0, numDos = 0, resultado = 0;
        
        System.out.println("¿Cúal es tu nombre?"); //Permite mostrar un menjase al usuario
        nombre = in.nextLine();
        /* Para guardar la data que introduzco el usuario se debe llamar un metodo segun el tipo dato:
            String ->  .nextLine()  
            Int    ->  .nextInt()    
        */
        System.out.println("Dame el primer valor para tu suma: ");
        numUno = in.nextInt();

        System.out.println("Dame el segundo valor para tu suma: ");
        numDos = in.nextInt();

        resultado = numUno + numDos;

        System.out.println("Hola " + nombre + ", el resultado de la Suma es: " + resultado);

    }
}