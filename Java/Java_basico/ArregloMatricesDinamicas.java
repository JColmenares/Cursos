import java.util.Scanner;

/**
 * ArregloMatricesDinamicas
 */
public class ArregloMatricesDinamicas {
    public static void main(String args[]) {
        int filas = 0, columnas = 0, contador = 1;
        Scanner entrada = new Scanner(System.in);

        System.out.print("¿Cuantas filas deseas?: ");
        filas = entrada.nextInt();

        System.out.print("¿Cuantas columnas deseas?: ");
        columnas = entrada.nextInt();

        int num [][] = new int[filas][columnas];

        for (int j = 0; j < filas; j++) {
            for (int i = 0; i < columnas; i++) {
                num[j][i] = contador;
                contador++;
                System.out.print("[" + num[j][i] + "]");
            }
            System.out.println("");
        }
    }
}