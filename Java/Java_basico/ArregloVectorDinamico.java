import java.util.Scanner;

/**
 * ArregloVectorDinamico
 * ****************************************
 *  Ejercicio: Realizar uns programa que contega un vector cuya longuitud sea proporcionado por el
 *             usuario, posteriormente solicitar al usuario los númeroes necesarios para llenar todas las 
 *             posiciones del vector, es decir, si el usuario solicito un arreglo de 10 posiciones, entonces el
 *             programa deberá de solicitar al usuario 10 números, y finalmente se debe de imprimir el arreglo 
 *             en la pantalla de la siguiente manera [5][54][12][65][6][54][867]
 * 
 */
public class ArregloVectorDinamico {
    public static void main(String args[]) {
        
        int loguitud = 0;
        Scanner entrada = new Scanner(System.in);

        System.out.print("¿Cuantos números deseas ingresar?: ");
        loguitud = entrada.nextInt();

        int num[] = new int[loguitud];

        for (int i = 0; i < num.length; i++) {
            System.out.print("Introduzca el número " + (i+1) + ":");
            num[i] = entrada.nextInt();
        }

        for (int i = 0; i < num.length; i++) {
            System.out.print("[" + num[i] + "] ");
        }
    }
}